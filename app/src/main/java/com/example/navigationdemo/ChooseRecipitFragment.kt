package com.example.navigationdemo


import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_choose_recipit.*

/**
 * A simple [Fragment] subclass.
 */
class ChooseRecipitFragment : Fragment(), View.OnClickListener {

    var navController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_choose_recipit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
        view.findViewById<Button>(R.id.next_btn).setOnClickListener(this)
        view.findViewById<Button>(R.id.cancel_btn).setOnClickListener(this)

    }

    override fun onClick(view: View?) {

        when (view!!.id) {
            R.id.next_btn -> {

                if (!TextUtils.isEmpty(input_recipient.text.toString())) {
                    val bundle = bundleOf("recipient" to input_recipient.text.toString())
                    navController!!.navigate(R.id.action_chooseRecipitFragment_to_specifyAmountFragment2, bundle)

                }else{
                    Toast.makeText(activity, "Please enter name of recipient", Toast.LENGTH_SHORT).show()
                }

//                navController!!.navigate(R.id.action_chooseRecipitFragment_to_specifyAmountFragment2)

            }
            R.id.cancel_btn -> {

                activity!!.onBackPressed()

            }
        }

    }


}
